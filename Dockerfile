FROM node:12
WORKDIR /home/node/app
COPY app /home/node/app
EXPOSE 8080
RUN npm install
CMD npm start